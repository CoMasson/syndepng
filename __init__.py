# copyright Creative Commons:  CC BY NC SA
# author Patrick Paroubek, pap@lisn.fr
# date April 05 2022

import sys
import io
from pprint  import pprint
import pandas as pd
import json
import pickle
from pathlib import Path
from configparser import ConfigParser

from stanza_to_list_of_indexed_objects import Stanza_Output
from bert_to_list_of_indexed_objects import BERT_Output
from dynprogalign import dynprogalign, alignment_info
from merge_annotations import *
from text_span_v2 import *
from noun_group_extractor import filter_noun_groups, print_mwus, print_rels

# for French
from flaubert_sliding_tokenizer import any_text_len_flaubertizer

#DEBUG=True
DEBUG=False

#------ graph drawing defs--------
from igraph import *

def make_graph( label_attr = 'lbl', root_label = '[GRAPH_ROOT]' ):
    g = Graph( directed = True )
    g.vs[ label_attr ] = []
    g.es[ label_attr ] = []
    root_v = g.add_vertex( lbl = root_label )
    return g

def add_labelled_vertice(  gr, l, last_v = False ):
    # gr is an igraph graph
    # l is the label (str) of the new vertex
    # s is the "start_attribute"
    global label_attr
    global root_label
    global dir_attr
    # since word may label nodes, there may be two nodes with the same label in the graph (see below)
    #if  not gr.vs.select( lbl=l ):
        # alternate a leading newline in the labels to avoid graphic overlap
    if last_v:
        l = '\n\n' + l
        last_v = False
    new_vertice = gr.add_vertex( lbl = l )
    new_v = len( gr.vs ) - 1
    assert( gr.vs[ new_v ].index == new_v )
## only used when all nodes have distinc labels
##    assert( new_vertice == gr.vs.select( lbl=l )[0] )
    assert( new_vertice in gr.vs.select( lbl=l ) )
    if DEBUG:
        print( 'add_labelled_vertice() node id {0} label {1}'.format( new_vertice.index, new_vertice['lbl'] ))
    return new_vertice
## only used when no two nodes have the same label (see above)
##    else:
##        existing_vertices = gr.vs.select( lbl=l )
##        assert( len( existing_vertices ) < 2 )
##        return existing_vertices[ 0 ]

def is_root( g, v ):
    # if (g.es.select( _source = root_index, _from = v.index)) or (g.es.select( _source = v.index, _from = root_index )):
    return v.index == 0
    
def color_idx( g, v, label_attr ):
    #print( 'DEBUG color_idx====>' + '{0}'.format(  v[ label_attr ] ))
    if is_root( g, v):
        return 2
    elif v[ label_attr ].split( '|' )[ 1 ].strip() in [ 'NOUN', 'PROPN' ]: 
        return 5
    elif v[ label_attr ].split( '|' )[ 1 ].strip()   == 'VERB': 
        return 2
    elif v[ label_attr ].split( '|' )[ 1 ].strip()   == 'NounGroup':
        return 3
    else:
        return 4

def edge_color_idx( g, e, label_attr ):
    #print( 'DEBUG edge_color_idx ====>' + '{0}'.format(  v[ label_attr ] ))
    if e[ label_attr ].split( '_' )[ 0 ] == 'NextW':
        return 6
    elif e[ label_attr ] == 'PartOfNounGroup':
        return 3
    else:
        return 0

def show( g, layout = None, bbox = None, v_size = None, lbl_size = None, margin = None, label_attr = 'lbl' ):
    visual_style = {}
    if v_size:
        visual_style["vertex_size"] = v_size
    ##g.vs["color"] = [color_dict[gender] for gender in g.vs["gender"]]
    color_dict = { 0:'black', 1:'white', 2:'red', 3:'green', 4:'cyan', 5:'orange', 6:'grey', 7:'purple', 8:'brown', 9:'Daffodil', 10:'yellow', 11:'magenta'}
    visual_style["vertex_color"] = [ color_dict[ k ] for k in list( map( lambda v : color_idx( g, v, label_attr), g.vs )) ]
    visual_style["vertex_label"] = g.vs[ str('lbl') ]
    #visual_style["edge_width"] = [1 + 2 * int(is_formal) for is_formal in g.es["is_formal"]]
    #visual_style["shape"] = 'rectangle'
    visual_style[ "vertex_label_dist" ] = 1
    if lbl_size:
        visual_style["label_size"] = lbl_size
    if layout:
        visual_style["layout"] = layout
    if bbox:
        visual_style["bbox"] = bbox
    if margin:
        visual_style["margin"] = margin
    visual_style["edge_color"] = [ color_dict[ k ] for k in list( map( lambda e : edge_color_idx( g, e, label_attr), g.es )) ]
    plot(g, **visual_style)

def make_word_node_label( m, doc ):
    assert( type( m ) is mwu )
    return m.text( doc ) + ' | ' + '_'.join( m.annotations[ 'w_upos' ] )

def l80( s ):
    res = '' ; i = 0 ; ln = 0 ; lbreak = False
    while i < len( s ):
        if ((ln % 80) == 0) and (ln > 0):
            lbreak = True
        if lbreak:
            if (s[ i ] == '\n') or (s[ i ] == ' '):
                res += '\n'
                ln = 0
                lbreak = False
            else:
                res += s[ i ]
                ln += 1
        else:
            res += s[ i ]
            if s[ i ] == '\n':
                ln = 0
            else:
                ln += 1
        i += 1
    return res

def display_file_graph( doc, show_graph = False, target_file = '/tmp/igraph_out.pdf' ):
    # node attributes
    graph_root_index = 0
    graph_root_vertex = None
    text = doc.content()
    graph_root_label = l80( doc.content() )
    #-------- graph drawing and display -------------
    relation_labels = []

    g = make_graph( label_attr = 'lbl', root_label = graph_root_label )
    graph_root_vertex = g.vs[ 0 ]
    mwu_vertex_map = { graph_root_label : graph_root_vertex }
    prev_vertex = None

    # first create the dependency root word node for the root of all the sentences
    dep_root_mwu_nm_lst = []
    for r_nm in doc.rels:
        r = doc.rels[ r_nm ]
        if DEBUG:
            print( 'r.typ=={0}'.format( r.typ ) )
        if r.typ == 'root':
            assert( r.src == r.trgt )
            dep_root_mwu_nm_lst.append( r.src.name )
    assert( len( dep_root_mwu_nm_lst ) > 0 )

    # then create the other word nodes in the same order as they appear in the sentence with the 'NextW' (next word) relation
    mwu_cnt = 1
    prev_vertex = None
    for m_nm, coff in sorted( [ (y.name, y.txtspans) for y in doc.mwus.values() ],
                          key = lambda x : x[ 1 ][ 0 ] ):
      if m_nm in dep_root_mwu_nm_lst:
          v = add_labelled_vertice( g, make_word_node_label( doc.mwus[ m_nm ], doc ) )
          mwu_vertex_map[ m_nm ] = v
          relation_labels.append( 'in_document'.format( mwu_cnt ) )
          g.add_edge( source = v , target= graph_root_vertex, lbl = relation_labels[ -1 ] )
          if prev_vertex:
              relation_labels.append( 'NextW_{0}'.format( mwu_cnt ) )
              g.add_edge( source = prev_vertex , target = v, lbl = relation_labels[ -1 ] )
          prev_vertex = mwu_vertex_map[ m_nm ]
      else:
          m = doc.mwus[ m_nm ]
          v = add_labelled_vertice( g, make_word_node_label( m, doc ) )
          mwu_vertex_map[ m_nm ] = v
          if prev_vertex == None:
              prev_vertex = v
          else:
              relation_labels.append( 'NextW_{0}'.format( mwu_cnt ) )
              g.add_edge( source = prev_vertex , target= v, lbl = relation_labels[ -1 ] )
              prev_vertex = v
      mwu_cnt += 1

    # using vertex_label= is equivalent to g.vs=...,  so to label your edges, use g.es=:
    # g.es["label"] = ["A", "B", "C"] or g.es["name"] = map(str, np.arange(N))

    # add the dependencies between the words (the root of the sentence is marked by a circular relation)
    for r_nm in doc.rels:
        r = doc.rels[ r_nm ]
        relation_labels.append( '{0}'.format( r.typ  ) )
        g.add_edge( source = mwu_vertex_map[ r.src.name ] , target = mwu_vertex_map[ r.trgt.name ], lbl = relation_labels[ -1 ] )
    g.es[ "label" ] = relation_labels

    for k in doc.constructions:
      msg = ''
      res_mwus = list( k.mwus.values())
      # sort on the first character pos of the first token
      res_mwus.sort( reverse=False, key=lambda x : x.txtspans[0][0] )
      for m in res_mwus:
        msg += (' ' + m.text( doc ) )
      if DEBUG:
          print( msg )
      #-----
      v = add_labelled_vertice( g, msg + ' | NounGroup' )
      for m in res_mwus:
        relation_labels.append( '{0}'.format( 'PartOfNounGroup'  ) )
        g.add_edge( source = v, target = mwu_vertex_map[ m.name ], lbl = relation_labels[ -1 ] )
    
    #lay= g.layout( 'rt', root=[0] )
    #lay = g.layout( 'fr' )  
    #lay = g.layout( 'tree', root=[0]  ) 
    #lay = g.layout( 'rt_circular', root=[0] ) 
    lay = g.layout( 'kk' )
    autocurve( g )  # prevent edges from overlapping
    #lay.rotate( 90 ) # horizontal versus vertical display

    if show_graph:
        show( g, layout=lay, bbox= (4096, 4096), margin=150, v_size = 10 )

    # view with xdot application
    dot = g.write( target_file + '.dot' )

    with open( target_file + '.dot', 'r' ) as of:
        the_dot_graph = of.read().replace( 'lbl=', 'label=' )
    with open( target_file + '.dot', 'w' ) as of:
        of.write( the_dot_graph )

 #------------------------- end of graph drawaing defs----


if __name__ == "__main__":
  conf_parser = ConfigParser( allow_no_value = True )
  conf_file = Path( __file__ ).parent / "config.ini"
  conf_parser.read( conf_file )

  the_file_lst = conf_parser.get( 'configuration', 'input_file_lst' )
  file_lst = []
  flst =  open( the_file_lst, 'r' ) 
  for f in flst:
    file_lst.append( f.strip() )
  flst.close()

  lang = conf_parser.get( 'configuration', 'lang' )
  assert( lang in [ 'en', 'fr' ] )

  #----------------------------stanza processing of the whole corpus
  stanza_func = Stanza_Output( lang = lang,
                               srcdir = Path( __file__ ).parent / conf_parser.get( 'configuration', 'input_dir' ),
                               file_lst = file_lst,
                               sufx = conf_parser.get( 'configuration', 'txt_sufx' ) )
  last_file_idx = stanza_func.curr_file_idx
 
  stanza_res = dict()
  tmp_list = []
  tok = stanza_func.next_tok()
  while tok:
    if DEBUG:
        print( '_________stanza________________' )
        pprint( tok )
        print( format( '\t{0}'.format( tok.annotations )) )
    tmp_list.append( tok )
    tok = stanza_func.next_tok()
    if not tok:
      if DEBUG:
          print( 'stanza processed file {0}'.format( stanza_func.file_lst[ stanza_func.curr_file_idx ] ))
      stanza_res[stanza_func.file_lst[ stanza_func.curr_file_idx ]] = tmp_list
      tmp_list = []
      if not stanza_func.next_file():
        break
      else:
        last_file_idx = stanza_func.curr_file_idx
        tok = stanza_func.next_tok()
        assert( tok )

  #----------------------------BERT processing of the whole corpus
  bert_res = dict()
  if lang == 'en':
      bert_func = BERT_Output( lang = 'en',
                               srcdir = conf_parser.get( 'configuration', 'input_dir' ),
                               file_lst = file_lst,
                               sufx = conf_parser.get( 'configuration', 'txt_sufx' ) )
      tmp_list = []
      tok = bert_func.next_tok()
      while tok:
        if DEBUG:
            print( '_________________________' )
            pprint( tok )
            print( format( '\t{0}'.format( tok.annotations )) )
        tmp_list.append( tok )
        tok = bert_func.next_tok()
        if not tok:
          if DEBUG:
              print( 'bert processed file {0}'.format( bert_func.file_lst[ bert_func.curr_file_idx ] ))
          bert_res[bert_func.file_lst[ bert_func.curr_file_idx ]] = tmp_list
          tmp_list = []
          if not bert_func.next_file():
            break
          else:
            last_file_idx = bert_func.curr_file_idx
            tok = bert_func.next_tok()
            assert( tok )
  else:
      assert( lang == 'fr' )
      for fnm in file_lst:
        with open( '{0}'.format( Path( __file__ ).parent / conf_parser.get( 'configuration', 'input_dir' )) + '/' + fnm + '.txt', 'rt' ) as inf:
            the_texte = inf.read()
            the_doc   = document( id = 'fnm', content = the_texte )
            bert_tokens = []
            try:
               if DEBUG:
                   print( '>>>>> STARTGING bert_tokens computation HERE' )
                   sys.stdout.flush()
               bert_tokens = any_text_len_flaubertizer( the_text = the_doc.ctnt,
                                                        model_name = 'flaubert/flaubert_large_cased' )
               if DEBUG:
                   print( '>>>>> bert_tokens computation DONE' )
                   sys.stdout.flush()
               assert( (type( bert_tokens ) is list) and (len( bert_tokens ) > 0))
               assert( False not in [ (type( x ) is indexed_object) for x in bert_tokens ] )
               bert_res[ fnm ] = bert_tokens
            except:
               print( 'ERROR exception raised during flaubert analysis' )
  if DEBUG:
      print("BERT Done!")
      
# ------ Realignement between stanza and bert tokens

  res_realignement = dict()
  for file_name in file_lst:
    aligner = dynprogalign( stanza_res[ file_name ] , bert_res[ file_name ] )
    realigned_streams = aligner.levenshtein_map()
    assert( (type( realigned_streams ) is tuple) and (len( realigned_streams ) == 2))
    ref_stream = merge_annotations( realigned_streams ) # effet de bord sur les annotations de ref du realigned stream
    res_realignement[ file_name ] = ref_stream
  if DEBUG:
      print("Realignement Done!")

#Indexed Object to MWU
  pointers_mwu = dict()
  sentence_tokens_ids = dict()
  res_indexed_object_to_mwu = dict()
    
  triplets = dict()
  noun_groups_res = dict()

  for fnm,realigned_streams in res_realignement.items():
    text_dir = (Path(conf_parser.get( 'configuration', 'input_dir' )) / fnm).with_suffix( conf_parser.get( 'configuration', 'txt_sufx' ))
    text = open(text_dir, 'r').read()
    doc = document( id = fnm, content = text, metadata = 'metadata')
    if DEBUG:
      print( 'in __init__() document is: {0}'.format( document ))
    indexed_object_to_mwu( doc, realigned_streams, pointers_mwu, sentence_tokens_ids )

    if DEBUG:
        print( 'name\ttext\t(start,end)\tstanzaId\tlemma\tPOS\thead\tdeprel')
        for m_nm in doc.mwus.keys():
          m = doc.mwus[ m_nm ]
          msg = '{0}\t{1}'.format( m.name, m.annotations[ 'w_text' ] )
          for sp in m.txtspans:
            msg += '\t({0},{1})'.format( sp[0], sp[1] )
          msg += '\twid={0}\t{1}\t{2}'.format(  m.annotations[ 'w_id' ], m.annotations[ 'w_lemma' ], m.annotations[ 'w_upos' ] )
          msg += '\tm{0}\t{1}'.format(  m.annotations[ 'w_head' ], m.annotations[ 'w_deprel' ] )
          if DEBUG:
              print( msg[0:min(300, len( msg ))] )      # print only the 300 first chars otherwise displayed information is too long with the token BERT vector contents
   
    get_relations( doc, pointers_mwu, sentence_tokens_ids )

    if DEBUG:
        print('_______the text content is:\n\n{0}'.format( doc.content() ))
        print_mwus( doc.mwus, doc )
        print_rels( doc.rels )
        print( '\n_______________________\n' )

    exclude_determiner_p = conf_parser.getboolean( 'configuration', 'exclude_determiner_p' )
    noun_groups_res = filter_noun_groups( doc, exclude_determiner_p )  # EXTRACT NOUN GROUPS

    if DEBUG:
        print( '%%%%%%%%%%%%%%%%%%%%%% in __init__() RESULTAT FINAL %%% \nlen( noun_groups_res)== {0}'.format( len( noun_groups_res) ))
    #-----
    for k in noun_groups_res:
      msg = ''
      res_mwus = list( k.mwus.values())
      # sort on the first character pos of the first token
      res_mwus.sort( reverse=False, key=lambda x : x.txtspans[0][0] )
      if DEBUG:
          print( [ x.txtspans[0][0] for x in list( k.mwus.values()) ] )
      for m in res_mwus:
        msg += (' ' + m.text( doc ) )
      if DEBUG:
          print( msg )
      if DEBUG:
          print( '\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n' )

    doc.constructions = noun_groups_res

    #----- dumping results to files
    output_dir = conf_parser.get( 'configuration', 'outdir' )

    with open( output_dir + doc.id + '_STANZA.csv', 'wt' ) as out_strm:
        doc.export_rels_to_csv( out_strm )
    
    with open( output_dir + doc.id + '_BERT.csv', 'wt' ) as out_strm:
        doc.export_mwus_bert_to_csv(  out_strm, headerp = True )

    with open( output_dir + doc.id + '_core_noun_groups.csv', 'wt' ) as out_strm:
        for k in noun_groups_res:
          msg = ''
          res_mwus = list( k.mwus.values())
          # sort on the first character pos of the first token
          res_mwus.sort( reverse=False, key=lambda x : x.txtspans[0][0] )
          for m in res_mwus:
            msg += (' ' + (m.text( doc ).replace('\t', ' ')))
          msg = msg.strip()
          msg += '\t'
          for m in res_mwus:
            msg += '{0} '.format( m.txtspans )
          msg = msg.strip()
          out_strm.write( msg + '\n' )
 
    display_file_graph( doc, conf_parser.getboolean( 'configuration', 'show_graph' ), target_file = output_dir + doc.id + '_igraph' )
          
    #-----
    
