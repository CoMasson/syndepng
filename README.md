# SynDepNg

Keywords: Natural Language Processing, Information Extraction, Dependency Parsing

A syntax dependency based core noun group extraction python program with token embedding alignment.
First version using Stanza parser and BERT embeddings for processing texts in Eenglish.

## Visuals
The igraph rendering of the enhanced dependency graph, full view for the sentence
>*We've not felt pricing pressure that's creating any concerns for us from a share standpoint with respect to the imports and then some of the domestic challenges, if you will, from an inflationary standpoint.*

is available here: [Sample enhanced dependency graph](https://gitlab.com/papLimsiCNRS/syndepng/-/blob/main/screenshots/test10_igraph_view.png)
and detailled view (links in grey are *next word relation*, i.e. explication of the sequence ordering of words in
the sentence), the links in green are for dependency based core noun groups and the links in black are Stanza
dependency relations (note the root of the sentence is indicated with a loop edge) is available here: 
[Detail of enhanced dependency graph](https://gitlab.com/papLimsiCNRS/syndepng/-/blob/main/screenshots/test10_igraph_view_details.png)

## Installation
This program has been tested with python3.7 and python3.8.
To install get and uncompress the source somewhere:
1. Check that you have Stanza installed [stanza](https://stanfordnlp.github.io/stanza/)
2. Get general BERT embeddings for English (the one used here comes from [BERT embedding](https://github.com/ROCmSoftwarePlatform))
3. Update the config.ini file according to your setup, in particular set the location of the BERT embedding files, the input file list which contain the list
of text files to analyze and the output directory where the result files will be saved.
# config.ini
~~~
[configuration]
modeling = ./bert/
global_out_sufx = .out
layers = -1
txt_sufx = .txt
bert_config_file = ./bert_data/uncased_L-10_H-128_A-2_dir/bert_config.json
max_seq_length = 128
init_checkpoint = ./bert_data/uncased_L-10_H-128_A-2_dir/bert_model.ckpt
vocab_file = ./bert_data/uncased_L-10_H-128_A-2_dir/vocab.txt
do_lower_case = True
batch_size = 32
use_tpu = False
master
num_tpu_cores = 8
use_one_hot_embeddings = False
#--- input data
# with trailing slash
input_dir = ./corpus/ 
input_file_lst = flst.txt
#--- output data
# with trailing slash
outdir = ./global_output_dir/  
stanza = True
spacy = False
#--- file graph viewing
show_graph = False
~~~
4. Load the file `import_lst.py`, if you have any error message install the missing library with pip3 install ...
5. Run the code with: `python3 __init__.py`

## Usage

If you have `flst.txt` (see point 3 above) that contains a two lines with the names `test9` and `test10` and there are two files
named `test9.txt` and `test10.txt in the `corpus` subdirectory.
`tesst9.txt contains the following sentence:

>*We've not felt pricing pressure that's creating any concerns for us from a share standpoint with respect to the imports and then some of the domestic challenges, if you will, from an inflationary standpoint.*

and `tesst10.txt contains the following sentences:

>*Whole the technology sector is no surprisingly, blooming, there is a lot of room  for catching up for the transportation and consumer services sectors.
We've not felt pricing pressure that's creating any concerns for us from a share standpoint with respect to the imports and then some of the domestic challenges, if you will, from an inflationary standpoint.*

Then the ouput will be spread among the following three files:
~~~
$ pwd
/home/xxx/SynDepNG/global_output_dir
$ ls -l
total 220
-rw-rw-r-- 1 pap pap 103808 Apr 10 02:14 test10_BERT.csv
-rw-rw-r-- 1 pap pap    389 Apr 10 02:14 test10_core_noun_groups.csv
-rw-rw-r-- 1 pap pap  13511 Apr 10 02:15 test10_igraph.dot
-rw-rw-r-- 1 pap pap  11914 Apr 10 02:14 test10_STANZA.csv
-rw-rw-r-- 1 pap pap  63673 Apr 10 02:05 test9_BERT.csv
-rw-rw-r-- 1 pap pap    235 Apr 10 02:05 test9_core_noun_groups.csv
-rw-rw-r-- 1 pap pap   7934 Apr 10 02:14 test9_igraph.dot
-rw-rw-r-- 1 pap pap   6925 Apr 10 02:05 test9_STANZA.csv
~~~

The file `test9_BERT.csv` contains the word of the text with their lexical information and BERT embedding vector in csv format.
~~~
#docid	mwu_name	mwu_type	mwu_text_spans	mwu_forms	w_lemma	w_upos	w_xpos	w_feats	BERT0	BERT1	BERT2	BERT3	BERT4	BERT5	BERT6	BERT7	BERT8	BERT9	BERT10	BERT11	BERT12	BERT13	BERT14	BERT15	BERT16	BERT17	BERT18	BERT19	BERT20	BERT21	BERT22	BERT23	BERT24	BERT25	BERT26	BERT27	BERT28	BERT29	BERT30	BERT31	BERT32	BERT33	BERT34	BERT35	BERT36	BERT37	BERT38	BERT39	BERT40	BERT41	BERT42	BERT43	BERT44	BERT45	BERT46	BERT47	BERT48	BERT49	BERT50	BERT51	BERT52	BERT53	BERT54	BERT55	BERT56	BERT57	BERT58	BERT59	BERT60	BERT61	BERT62	BERT63	BERT64	BERT65	BERT66	BERT67	BERT68	BERT69	BERT70	BERT71	BERT72	BERT73	BERT74	BERT75	BERT76	BERT77	BERT78	BERT79	BERT80	BERT81	BERT82	BERT83	BERT84	BERT85	BERT86	BERT87	BERT88	BERT89	BERT90	BERT91	BERT92	BERT93	BERT94	BERT95	BERT96	BERT97	BERT98	BERT99	BERT100	BERT101	BERT102	BERT103	BERT104	BERT105	BERT106	BERT107	BERT108	BERT109	BERT110	BERT111	BERT112	BERT113	BERT114	BERT115	BERT116	BERT117	BERT118	BERT119	BERT120	BERT121	B`ERT122	BERT123	BERT124	BERT125	BERT126	BERT127
test9	T_1_1	Token	[(0,2)]	['We']	['we']	['PRON']	['PRP']	['Case=Nom|Number=Plur|Person=1|PronType=Prs']	BERT_Representation	[[0.383424, -1.670362, 1.67679, -1.099556, 0.137687, 0.670812, 0.435612, -2.043581, 1.434762, 0.15255, -1.059198, -0.110474, 1.338165, 0.385174, 0.754608, 1.198342, -0.231709, 0.975582, -0.702772, 0.242351, -0.881765, -3.327957, -0.988748, 1.08393, 0.381701, -0.60066, 0.82446, -0.254685, -0.920888, -0.131939, 0.291022, -0.201916, 0.47391, -1.641064, -0.636979, -0.72282, -1.050344, 0.105398, -1.035655, 0.29852, 0.451961, -2.117347, -0.773571, 0.540412, 0.656369, -2.113518, -0.084752, 0.921108, 0.99314, -0.351042, -1.835895, -0.242733, 1.504235, -1.418791, 0.15806, 0.979626, 1.814518, -0.790078, 1.48896, 0.333406, -0.524353, -0.111269, 0.714817, -1.156832, -0.305875, -0.489428, -0.089598, -0.069432, 0.721682, -0.310165, 0.380796, -1.378259, 1.64895, 0.611574, 0.059321, 1.643545, -0.222306, -0.609926, 1.016387, 0.647827, -0.916278, 0.030797, -0.454015, 1.317685, 0.561011, 1.846593, -0.253704, -0.215035, -1.480065, 2.435743, -0.810187, 0.977122, -0.101741, -1.324061, -0.725932, 0.187656, 0.369138, -1.621591, -0.959869, 0.890864, -0.79033, -0.594246, 0.756975, 1.05054, 0.487005, -0.422385, -0.401605, -0.042423, -1.350504, -0.27116, 1.170253, 0.334007, 1.648986, 0.24735, -1.897458, 0.167408, 1.537278, 0.689576, -0.171752, 0.022826, 0.329153, 0.070625, -0.320316, 1.380849, -1.292121, 1.270118, -0.233812, 0.647781]]
test9	T_1_2	Token	[(2,5)]	['\'ve']	['have']	['AUX']	['VBP']	['Mood=Ind|Tense=Pres|VerbForm=Fin']	BERT_Representation	[[0.345024, -1.197026, 0.843859, -1.742802, 0.54002, 1.214539, -1.392085, 0.368899, 1.201283, -1.360669, -0.014062, -0.03986, 0.643658, 1.429225, 1.78118, 1.116533, -1.494746, -0.647508, -0.462543, 0.271089, 1.240135, -1.117157, -0.30159, -0.077182, -0.338065, -0.307762, 1.536621, -1.613221, -0.415731, -0.815862, 0.263743, -0.994903, 1.47487, -0.610597, -0.764012, -1.219725, 0.080204, -0.052171, -1.002666, 1.029105, 0.712481, -0.032581, -0.742932, 0.574546, 0.148878, -0.930645, 0.957418, 1.749964, -0.466908, -1.110232, -1.212721, -0.146946, -0.030791, -2.610131, -1.455015, -0.025814, 0.65167, -0.693263, 2.781491, 0.235409, -0.189015, -0.874432, 1.622227, -1.336151, 0.541648, -1.179405, 0.780583, 0.783, 0.979515, -1.328181, 1.963278, 0.474475, -0.086529, 1.081086, 0.519417, 1.338688, 0.097913, -0.213993, 0.847831, -0.590297, -1.112, 0.837578, -0.356418, 0.309346, 1.505095, 1.240919, 0.75646, -0.631456, 0.002837, -0.15702, 1.397276, 0.264743, 0.944122, -0.058299, -0.420587, -0.892278, -0.844936, -1.394186, -0.824766, 1.521667, 0.795654, -1.707281, 1.566072, 0.424674, -1.117376, -0.092643, -0.815359, -1.232946, -1.328044, -0.157431, -1.333822, 0.947076, 0.367668, 0.299408, -1.656992, -0.679148, 0.609509, 0.469063, -0.107052, 0.627812, -1.160973, 0.461284, 0.048885, 0.838304, 0.631824, 1.949009, -0.953911, -0.764929], [-0.057014, -0.246911, 0.439452, -0.86047, 1.494841, 0.430782, 0.302128, -1.822404, -0.780953, 0.416879, -0.577125, -0.88638, 0.753484, 0.94442, 0.596322, 0.823663, -1.537176, -0.016133, -1.340188, 0.05604, 0.669741, -2.208144, -0.754248, -0.641856, -0.447742, -1.096055, -0.572278, -1.375277, 0.498649, -0.562272, -0.836302, -0.660271, 2.701333, -0.947045, -0.462814, 0.429632, 0.210482, -0.847813, -0.544294, 0.953391, 0.394176, -0.085084, -0.003938, -0.121002, 0.977692, 0.273879, 0.488204, 1.435193, 0.429226, -0.543681, -1.341539, 1.106205, -0.472062, -1.163118, -1.356686, 0.527128, 1.885916, -0.370066, 2.607375, -0.425255, -0.169255, 0.38488, 0.495858, -0.547642, 1.216267, 0.550714, -0.958489, 1.41039, -0.420071, 1.116356, 2.710654, -1.291848, 1.598238, 0.759537, -0.854394, 1.353614, 0.451102, -0.134471, -0.039593, 0.656297, -0.53158, 0.187751, 0.358546, 0.766343, -0.642173, 1.631438, -1.178268, -1.845864, -1.251718, 1.858743, -0.063131, 1.509771, 0.991262, -1.568062, -0.227338, 0.570512, 1.36934, -0.149953, -0.256458, 0.730247, -0.691412, -1.224968, -0.175546, -0.385067, 1.900567, 0.399542, -0.408451, -2.337223, -0.958935, -0.18356, 0.753211, 0.247803, -0.17719, 0.39795, -1.508858, -0.184939, 1.290759, -0.82547, -1.424359, 0.780408, -0.374774, 0.292759, -0.198504, -1.225029, -1.213158, 0.568988, 0.074807, 0.360459]]
...etc....
~~~

The file `test9_core_noun_groups.csv`contains the dependency based core noun groups word forms with their character offsets of their components in csv format.
~~~
pricing pressure	[(15, 22)] [(23, 31)]
concerns	[(52, 60)]
share standpoint	[(75, 80)] [(81, 91)]
respect	[(97, 104)]
imports	[(112, 119)]
domestic challenges	[(141, 149)] [(150, 160)]
inflationary standpoint	[(183, 195)] [(196, 206)]
~~~

The file `test9_STANZA.csv` contains the word of the text with the dependency relations in tabular format, one relation per line, with the detailed source word informaion, the type of the dependency and finaly the detailed target word information.
~~~
test9	R	T_1_1	Token	[(0,2)]	['We']	['we']	['PRON']	['PRP']	['Case=Nom|Number=Plur|Person=1|PronType=Prs']	R_0	nsubj	T_1_4	Token	[(10,14)]	['felt']	['feel']	['VERB']	['VBN']	['Tense=Past|VerbForm=Part']
test9	R	T_1_2	Token	[(2,5)]	['\'ve']	['have']	['AUX']	['VBP']	['Mood=Ind|Tense=Pres|VerbForm=Fin']	R_1	aux	T_1_4	Token	[(10,14)]	['felt']	['feel']	['VERB']	['VBN']	['Tense=Past|VerbForm=Part']
test9	R	T_1_3	Token	[(6,9)]	['not']	['not']	['PART']	['RB']	[None]	R_2	advmod	T_1_4	Token	[(10,14)]	['felt']	['feel']	['VERB']	['VBN']	['Tense=Past|VerbForm=Part']
test9	R	T_1_4	Token	[(10,14)]	['felt']	['feel']	['VERB']	['VBN']	['Tense=Past|VerbForm=Part']	R_3	root	T_1_4	Token	[(10,14)]	['felt']	['feel']	['VERB']	['VBN']	['Tense=Past|VerbForm=Part']
test9	R	T_1_5	Token	[(15,22)]	['pricing']	['pricing']	['NOUN']	['NN']	['Number=Sing']	R_4	compound	T_1_6	Token	[(23,31)]	['pressure']	['pressure']	['NOUN']	['NN']	['Number=Sing']
test9	R	T_1_6	Token	[(23,31)]	['pressure']	['pressure']	['NOUN']	['NN']	['Number=Sing']	R_5	obj	T_1_4	Token	[(10,14)]	['felt']	['feel']	['VERB']	['VBN']	['Tense=Past|VerbForm=Part']
test9	R	T_1_7	Token	[(32,36)]	['that']	['that']	['PRON']	['WDT']	['PronType=Rel']	R_6	nsubj	T_1_9	Token	[(39,47)]	['creating']	['create']	['VERB']	['VBG']	['Tense=Pres|VerbForm=Part']
~~~

The file `test9_igraph.dot` contains the representation of the graph in `dot` format, which can be viewed with a capable dot graph viewer, like `xdot` (https://pypi.org/project/xdot/).
~~~
/* Created by igraph 0.9.6 */
digraph {
  0 [
    label="We've not felt pricing pressure that's creating any concerns for us from a share\nstandpoint with respect to the imports and then some of the domestic challenges,\nif you will, from an inflationary standpoint.\n"
  ];
  1 [
    label="We | PRON"
  ];
  2 [
    label="'ve | AUX"
  ];
  3 [
    label="not | PART"
  ];
  4 [
    label="felt | VERB"
  ];
  5 [
    label="pricing | NOUN"
  ];
~~~

## License
The files containes here have different copyright notices associated among: Apache LICENSE-2.0, Creative Commons, CC BY-NC-SA, MIT License.


