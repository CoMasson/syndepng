## MIT License
##
## Copyright (c) 2019 aakorolyova
##
## Permission is hereby granted, free of charge, to any person obtaining a copy
## of this software and associated documentation files (the "Software"), to deal
## in the Software without restriction, including without limitation the rights
## to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
## copies of the Software, and to permit persons to whom the Software is
## furnished to do so, subject to the following conditions:
##
## The above copyright notice and this permission notice shall be included in all
## opies or substantial portions of the Software.
##
## HE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
## OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
## SOFTWARE.
## Note: A large part of this file comes initially from (2019):
## https://github.com/aakorolyova/DeSpin/blob/master/text_span.py
## with additions in 2020 by Maha BOUZAIENE <bouzaiene.maha@gmail.com>
## and Patrick PAROUBEK <pap@limsi.fr>
## Updated for dependency based core noun group extraction by Patrick Paroubek on April 05 2022

import sys
import glob
import codecs
import os
import collections
import io
from copy import deepcopy
import re

from functools import partial
from collections import defaultdict

from pprint import pprint

import csv

import pdb

from itertools import permutations, count
from functools import reduce

#DEBUG = True
DEBUG = False

DEBUG_SHORT_REP_ANNOTS = True
#DEBUG_SHORT_REP_ANNOTS = False

# utility function for forward compatibility, if using unification library
def name( x ):
    return x.name

#----------- 
    
class mwu( object ):
    #--- class variables 
    default_mwu_name_prefix = 'MWU'
    default_mwu_cnt = count( start = 0, step = 1 ) 
    # txsps is a list of pairs of character offsets e.g.  mwu( nm='MWU_4', txtsps=[ (13300 , 13320)])
    # example of mwu structure:
    # m1=mwu( nm='MWU_4', txtsps=[ (13300 , 13320)], annotations={'comments':'ceci est un test', 'POS_GRACE':'Ncms'})
    
    def __init__( self, nm = None, txtsps = None, typ = None, annotations = None ):
        #--- name.
        if( nm is None ):
            self.id = next( mwu.mwu_cnt )
            self.name = mwu.default_mwu_name_prefix + '_' + '{0}'.format( self.id );
        else:
            self.name = nm
        if( txtsps is None ):
             self.txtspans = []
        else:
            self.txtspans = txtsps
        #--- annotations global to the mwu (attribute value associations in a dictionary).
        if( annotations is None ):
            self.annotations = {}
        else:
            self.annotations = annotations
        if( typ is None ):
             self.typ = ''
        else:
            self.typ = typ

    def text_n( self, doc, n ):
        tsp = self.txtspans[ n ]
        assert( type( tsp ) is tuple )
        return( doc.span( tsp[0], tsp[1] ))
                
    def text( self , doc, sep = None ):
        res = ''
        for i in range( 0, len( self.txtspans ) ):
                if( sep is None ):
                    res += self.text_n( doc, i )
                else:
                    if( i > 0 ):
                        res += sep
                    res += self.text_n( doc, i )  
        return( res )

    def span_n( self, n ):
        if( n < len( self.txtspans) ):
            return( self.txtspans[ n ] )
        else:
            return( None )

    def __repr__( self ):
        res = 'mwu( nm=' + name( self ).__repr__() +  ', txtsps=['
        n = 0
        for sp in self.txtspans:
            res += sp.__repr__()
            ##            res += 'wnts( \'SPAN\', ' +  str( sp[0] ) + ',' + str( sp[1] ) + ')'
            n += 1
            if( n < len( self.txtspans )):
                res += ', '
        res +=  ']'
        res += ', typ= {0}'.format( self.typ.__repr__() )
        if self.annotations:
            res += ', annotations={0}'.format( self.annotations.__repr__() )
        if DEBUG_SHORT_REP_ANNOTS:
            res = res[ 0: min(200, len( res)) ] + ' ETC...'
        res += ')'
        return( res ) 

    def __str__( self ):
        res = '---mwu:\n\tname= ' + str( name( self ) )
        res += '\n\ttextspans is: ' + str( self.txtspans )
        res += '\n\ttyp is: ' + str( self.typ )
        if DEBUG:
            res += '\n\tannotations is: ' + str( self.annotations )[0:min(200, len( str( self.annotations )))] + ' ETC...'
        else:
            res += '\n\tannotations is: ' + str( self.annotations )
        res += '\n'
        return( res )

    def size( self ):
        return( len( self.txtspans ) )
    
         #---- annotations of the whole mwu)
    def set_annot( self, aspect, value ):
        self.annotations[ aspect ] = value

    def get_annot( self, aspect ):
        return( self.annotations[ aspect ] )

    def append( self, m ):
        # append all the text spans of m to the list of self.txtspans
        # and incorporate annotations of m onto self.annotations
        if len( m.txtspans ) > 0:
            for s in m.txtspans:
                self.txtspans.append( deepcopy( s ))
            self.annotations.update( m.annotations )
        return self

    def overlap( self, mwu2 ):
        if self.txtspans[0][0] < mwu2.txtspans[0][0]:
            first = self
        else:
            first = mwu2
        if self.txtspans[-1][-1] > mwu2.txtspans[-1][-1]:
            last = self
        else:
            last = mwu2
        return (first == last) or (first.txtspans[-1][-1] > last.txtspans[0][0])
            
        
#--- end of mwu class

class rel( object ):
    #--- class variables 
    default_rel_name_prefix = 'R'
    default_rel_cnt = count( start = 0, step = 1 ) 
    def __init__( self, nm = None, src = None, trg = None, typ = None ):
        assert( (type( nm ) is str) or (nm  is None))
        assert( type( src is mwu) or (type( src ) is relation))
        assert( type( trg is mwu) or (type( trg ) is relation))
        assert( type( typ is str) )
        self.id = next( rel.default_rel_cnt )
        if( nm is None ):
            self.name = rel.default_rel_name_prefix + '_' + '{0}'.format( self.id );
        else:
            self.name = nm
        self.src = src 
        self.trgt = trg 
        self.typ = typ

    def __repr__( self ):
        res = 'rel( nm=' + name( self ).__repr__() + ', src=' +  self.src.__repr__() + ', trg=' + self.trgt.__repr__() + ', typ=' + self.typ.__repr__() + ')'
        return( res )

    def __str__( self ):
        sout = 'rel: nnm= ' + str(name( self )) + ' typ= ' +  str(self.typ) + ' src= ' +  str(self.src)  + ' trg= ' + str(self.trgt) + '\n'
        return( sout )

    
class relation( rel ):
    def __init__( self, nm=None, src=None, trg=None, annotations = None, typ = None ):
         assert( (type( nm) is str) or (nm is None))
         assert( (type( src ) is mwu) or (type( src ) is relation) )
         assert( (type( trg ) is mwu) or (type( trg ) is relation) )
         assert( (type( typ ) is str) )
         if( annotations is None ):
             self.annotations = {}
         else:
             self.annotations = annotations
                 
         rel.__init__( self, nm,  src, trg, typ )
         
     #---- annotations of the whole mwu)
    def set_annot( self, aspect, value ):
        self.annotations[ aspect ] = value

    def get_annot( self, aspect ):
        return( self.annotations[ aspect ] )

    def __repr__( self ):
        res = 'rel( nm=' + name( self ).__repr__() + ',src=' +  self.src.__repr__() + ', trg=' + self.trgt.__repr__() + ', typ=' + self.typ.__repr__()
        if self.annotations:
            res += 'annotations=' + self.annotations.__repr__()
        res += ')'
        return( res )

    def __str__( self ):
        sout = 'rel: nnm= ' + str(name( self )) + ' typ= ' +  str(self.typ) + ' src= ' +  str(self.src)  + ' trg= ' + str(self.trgt) + '\n'
        return( sout )
    

#--- end of class relation    

class construction( object ):
    # a construction is a (possibly empty) set of mwus and a (possibly empty) set of relations
    # with the condition that one of these two sets is not empty.
    # In addition there are two sets of optional mwus and optional relations that may be empty,
    # note that both of them can be empty.
    #--- class variables 
    default_construction_name_prefix = 'R'
    default_construction_cnt = count( start = 0, step = 1 )
    
    def __init__( self, nm = '', typ = '',  mwus = {}, rels = {}, opt_mwus = {}, opt_rels = {}):
        assert(  (type( mwus ) is list) or (type( mwus) is dict) )
        assert( (type( nm ) is str) )

        self.id = next( construction.default_construction_cnt )
        self.name     = nm
        self.typ      = typ
        self.mwus  = mwus
        self.rels  = rels
        self.opt_mwus =opt_mwus
        self.opt_rels = opt_rels

        if type( mwus ) is list:
            assert( False not in [((type( m ) is mwu)) for m in mwus ])
            for e in mwus:
                self.mwus[ name( e ) ] = e
        else:
            assert( type( mwus ) is dict)
            assert( False not in [ (type( m ) is mwu) for m in mwus.values() ])
            self.mwus = mwus
                
        if type( rels ) is list:
            assert( False not in [ (type( m ) is rel) for m in rels ])
            for e in rels:
                self.rels[ name( e ) ] = e
        else:
            assert( type( rels ) is dict)
            assert( False not in [ (type( m ) is relation) for m in rels.values() ])
            self.rels = rels

    def __repr__( self ):
        sout = 'construction( nm=' +  name( self ).__repr__() + ', rels=['
        n = 0
        for r in self.rels:
            sout += r.__repr__()
            n += 1
            if( n < len( self.rels ) ):
                sout += ', '
        sout += ']'
        return( sout )  

    def __str__( self ):
        sout = 'construction: name= ' + str( name( self )) + 'rels='
        for r in self.rels:
            sout += r.__str__()
        sout += '\n'
        return( sout )
    
    #--- adding elements

    def add_elt_to_store( self, e, store = None ):
        assert( store in [ 'mwus', 'rels' ] )
        assert( (type( e ) is mwu) or (type( e ) is relation) )
        if type( e ) is mwu:
            self.mwus[ e.name ] = e
        elif type( e ) is relation:
            self.rels[ e.name ] = e
        else:
            sys.stderr.write( 'ERROR element {0} has a wrong type {1} for being added to construction {2}'.format( e, type( e ), self.name ))
            assert( 0 )

    def add_mwu( self, m ):
        self.add_elt_to_store( m, 'mwus' )

    def add_rel( self, r ):
        self.add_elt_to_store( r, 'rels' )

    #--- getting elements

    def get_mwu( self, k = None ):
        assert( k is not None)
        if k in self.mwus.keys():
            return self.mwus[ k ]
        else:
            return None

    def find_mwu_containing_span( self, s ):
        assert( (type( s ) is tuple) and (len(s) == 2) and (type(s[0]) is int) and (type(s[1]) is int) and (s[0] < s[1]) )
        for (nm, mw) in self.mwus.items():
                if( mw.contains_span_p( s ) ):
                    return( mw )
        return( None )

    def find_mwus_with_type( self, typ ):
        res = []
        for m in self.mwus:
            if m.typ == typ:
                res.append( m )
        return res

    #--- removing elements

    def remove_element( self, e, store ):
        assert( (type( store ) is dict ) and ((type( e ) is mwu) or (type( e ) is list) or (type( e ) is dict) ) )
        if type( e ) is str:
            if e in self.store.keys():
                del self.store[ e ]
            else:
                pass
        else:
            if e.name in store:
                del self.store[ e.name ]
            else:
                pass
            
    def remove_mwu( self, m ):
        assert( (type( m ) is str ) or (type( m ) is mwu) )
        self.remove_element( m, self.mwus )

    def remove_rel( self, r ):
        assert( (type( m ) is str ) or (type( m ) is rel) )
        self.remove_element( r, self.relations )

    #------ optional part -----------

    def add_mwu_opt( self, m ):
        assert( (type( m ) is mwu) or (type(m) is list) or (type(m) is dict) )
        if( type( m ) is list ):
            assert( False not in [ (type( x ) is mwu) for x in m ])
            for e in m:
                self.mwus_opt[ e.name ] = e
        elif type( m ) is dict:
            assert( False not in [ (type( x ) is mwu) for x in m.values() ])
            self.mwus_opt.update( m )
        else:
            assert( type( m ) is mwu )
            self.mwus_opt[ m.name ] = m
        
    def add_rel_opt( self, r ):
        assert( (type( r ) is relation) or (type(r) is list) or (type( r) is dict ) )
        if( type( r ) is list ):
            for e in r:
                self.rels_opt[ e.name ] = e
        elif type( r ) is dict:
            self.rels_opt.update( r )
        else:
            assert( type( r ) is relation )
            self.rels_opt[ r.name ] = r
            
#----- end of class construction
            
class document( construction ):
    def __init__(self, id='', content='', metadata='', multi_word_units={}, relations={}, constructions={} ):
        if( type(metadata) is not str ):
            raise ValueError
        else:
             self.metadata = metadata
        if(  type(content) is not str ):
             raise ValueError
        else:
             self.ctnt = content
        # potentialy a document has multi-word units (mwus) annotations, relations (between two mwus, a source and a target)
        # and constructions (sets of relations)
        construction.__init__( self, nm = id,  mwus = multi_word_units, rels = relations  )
        self.mwus = {}  # WARNING: this initialisation is required here, the superclass __init__() call above is not enough to reset this data member
        self.rels = {}  # WARNING: this initialisation is required here, the superclass __init__() call above is not enough to reset this data member
        self.id = id # for backward compatibility
        self.constructions = constructions

    def len( self ):
        return( len( self.ctnt ) )

    def byte_len( self ):
        return( len( self.ctnt.encode('utf8') ))

    def content( self ):
        return( self.ctnt )

    def span( self, first, last ):
        return( self.ctnt[ first:last ] )

    def __repr__( self ):
        res = 'document( ident=\'' + self.id + '\''
        res += '\t\n, content=' + self.ctnt.__repr__()
        res += '\t\n, metadata=' + self.metadata.__repr__()
        res += '\t\n, multi_word_units = ['
        i=0
        for m_nm in self.mwus.keys():
            if( i > 0 ):
                res += ', '
            i += 1
            res += self.mwus[ m_nm ].__repr__()
        res += ']\n'
        res += '\t, relations= ['
        i = 0
        for r_nm in self.rels.keys():
            if( i > 0 ):
                res += ', '
            i += 1
            res += self.rels[ r_nm ].__repr__()
        res += ']\n'
        res += '\t, constructions= ['
        i = 0
        for k_nm in self.kstructs:
            if( i > 0 ):
                res += ', '
            i += 1
            res += slef.kstructs[ k_nm ].__repr__()
        res += '] )\n'
        return( res )

    def __str__( self ):
        res = 'document is:' + '\tid= ' + str( self.id )
        res += '\tmetadata= ' + str( self.metadata )
        res += '\tlen(self.ctnt)= ' + str( len( self.ctnt ))
        res += '\tlen(mwus)=' + str( len(self.mwus) )
        res += '\tlen(rels)=' + str( len(self.rels) )
        return( res )

    #------- file io

    def read_text( self, path ):
        assert( (type( path ) is str) and (path != '') )
        buffer_sz = 100
        in_strm = codecs.open( path, mode='r', encoding='utf-8', errors='strict', buffering = -1)
        assert( in_strm )
        in_data = u''
        for l in in_strm.readlines( buffer_sz ):
            in_data += l
        self.ctnt = self.ctnt + in_data
        in_strm.close()

    def write_text( self, path ):
        assert( (type( path ) is str) and (path != '') )
        out_strm = codecs.open( path, mode='w', encoding='utf-8', errors='strict', buffering = -1)
        assert( out_strm )
        out_strm.write( self.ctnt )
        out_strm.close()

    def mwu_to_csv_str( self, m ):
        def escape_metachar( s ):
            return s.replace( '\\', '\\\\').replace( '\n', ' ' ).replace( '\t', ' ' ).replace( "'", "\\'" )

        def nospace( s ):
            return re.sub('\ |\t|\n', '', s)
        out_msg = ''
        out_msg = '{0}\t{1}\t'.format( name( m ), m.typ )
        out_msg += nospace( '{0}'.format( m.txtspans ) )
        out_msg += '\t['
        i = 0
        for sp in m.txtspans:
            if i != 0:
                out_msg += ','
            out_msg += '\'{0}\''.format( escape_metachar( self.ctnt[ sp[0]:sp[1] ]))
        out_msg += ']\t'
        ## POS annotations
        if 'w_lemma' in m.annotations.keys():
            out_msg += '{0}\t'.format( m.annotations[ 'w_lemma' ] )
        else:
            out_msg += '-\t'
        if 'w_upos' in m.annotations.keys():
           out_msg += '{0}\t'.format( m.annotations[ 'w_upos'  ] )
        else:
            out_msg += '-\t'
        if 'w_xpos' in m.annotations.keys():
            out_msg += '{0}\t'.format( m.annotations[ 'w_xpos'  ] )
        else:
            out_msg += '-\t'
        if 'w_feats' in m.annotations.keys():
            out_msg += '{0}'.format( m.annotations[ 'w_feats' ] )
        else:
            out_msg += '-'
        return out_msg

    def export_mwus_to_csv( self, out_strm ):
        # export all existing mwus from the current doc into out_strm  in csv format
        # M/R is a class label for the current entry M=mwu and R=relation
        header = '{0}\t{1}\t'.format('#docid', 'M/R')
        header += '{0}\t{1}\t{2}\t{3}\t'.format( 'src_name', 'src_type', 'src_text_spans', 'src_text' ) # src mwu
        header += '{0}\t{1}\t{2}\t{3}\t'.format( 'src_lemma', 'src_upos', 'src_xpos', 'src_wfeats' )  # POS src
        header += '{0}\t{1}\t'.format( 'rel_name', 'rel_type' ) # relation
        header += '{0}\t{1}\t{2}\t{3}\t'.format( 'trgt_name', 'trgt_type', 'trgt_text_spans', 'src_text' ) # trgt mwu
        header += '{0}\t{1}\t{2}\t{3}\t'.format( 'trgt_lemma', 'trgt_upos', 'trgt_xpos', 'trgt_wfeats')  # POS trgt
        out_strm.write( header )
        out_strm.write( '\n' )
        for m_nm in self.mwus.keys():
            m = self.mwus[ m_nm ]
            out_strm.write( '{0}\t{1}\t'.format( self.id, 'M'))
            out_strm.write( self.mwu_to_csv_str( m ) )
            out_strm.write( '\t{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\n'.format( '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'))

    def export_rels_to_csv( self, out_strm ):
        # export all existing relations from the current doc into out_strm  in csv format
        # M/R is a class label for the current entry M=mwu and R=relation
        for r_nm in self.rels.keys():
                r = self.rels[ r_nm ]
                out_strm.write( '{0}\t{1}\t'.format( self.id, 'R'))
                out_strm.write( self.mwu_to_csv_str( r.src ))
                out_strm.write( '\t{0}\t{1}\t'.format( name( r ), r.typ ))
                out_strm.write( self.mwu_to_csv_str( r.trgt ))
                out_strm.write( '\n' )
        out_strm.flush()

##    # pap 20210813
    def export_mwus_bert_to_csv( self, out_strm, headerp = False ):
        ## export mwus, their Stanza POS information and distilled BERT embeddings
        if headerp:
            header = '\t'.join( [ '#docid', 'mwu_name', 'mwu_type', 'mwu_text_spans', 'mwu_forms', 'w_lemma' , 'w_upos', 'w_xpos', 'w_feats'] )
            for i in range( 0, 128 ):
                header += '\tBERT{0}'.format( i )
            out_strm.write( header )
            out_strm.write( '\n' )
        for m_nm in self.mwus.keys():
            m = self.mwus[ m_nm ]
            if m.typ == 'Token':
                out_strm.write( self.id )
                out_strm.write( '\t' )
                out_strm.write( self.mwu_to_csv_str( m ) )
                if 'BERT_Representation' in m.annotations.keys():
                    #out_strm.write( 'BERT_Representation={0}\t'.format( m.annotations[ 'BERT_Representation' ] ))
                    for x in m.annotations[ 'BERT_Representation' ]:
                        out_strm.write( '\t{0}'.format( x ) )
                out_strm.write( '\n')
        out_strm.flush()
            
#--- end of class document





          


