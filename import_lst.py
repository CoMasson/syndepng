from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from collections import defaultdict
from configparser import ConfigParser
from copy import deepcopy
from functools import partial
from functools import reduce
from igraph import *
from itertools import permutations, count
from math import inf as maxint
from pathlib import Path
from pprint import pprint 
from scipy.spatial.distance import cosine,dice
from sklearn.metrics.pairwise import cosine_similarity
from statistics import median
import codecs
import collections
import csv
import glob
import io
import itertools
import json
import logging
import matplotlib.pyplot as plt
directoryFichierConfig = Path( __file__ ).parent / "config.ini"
parser = ConfigParser( allow_no_value = True )
parser.read( directoryFichierConfig )
sys.path.append( parser.get('configuration','modeling' ) )
import modeling
import numpy
import os
import pandas as pd
import pdb
import pickle
import re
import shutil
import stanza
import sys
import tensorflow as tf
import time
import tokenization
from transformers import FlaubertModel, FlaubertTokenizer

