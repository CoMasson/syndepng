## This code written in 2020 by Maha BOUZAIENE <bouzaiene.maha@gmail.com> is licensed under CC BY-NC-SA
## This license allows reusers to distribute, remix, adapt, and build upon the material in any medium
## or format for noncommercial purposes only, and only so long as attribution is given to the creator.
## If you remix, adapt, or build upon the material, you must license the modified material under
## identical terms. 
## CC BY-NC-SA includes the following elements:
## BY – Credit must be given to the creator
## NC – Only noncommercial uses of the work are permitted
## SA – Adaptations must be shared under the same terms
## For further information see https://creativecommons.org/about/cclicenses/
## or look into the directory: ./copyright_NOTICES
## Modified by pap on April 05 2022 for dependency based noun group core extraction

import sys

from pathlib import Path
from dynprogalign import indexed_object
from text_span_v2 import mwu, relation, construction, document
from pprint import pprint
from copy import deepcopy

from configparser import ConfigParser
conf_parser = ConfigParser( allow_no_value = True )
directoryFichierConfig = Path( __file__ ).parent / 'config.ini'
conf_parser.read( directoryFichierConfig )
STANZA = conf_parser.getboolean( 'configuration', 'stanza' )
SPACY  = conf_parser.getboolean( 'configuration', 'spacy' )

DEBUG = True
#DEBUG = False

def merge_annotations( aligned_streams = None,
                       annotation_mixer = lambda akey, aref_val, ahyp_val : aref_val,
                       hyp_lst_annotation_mixer = lambda akey, ahyp_value_lst : (akey, ahyp_value_lst) ):
        assert( aligned_streams is not None )
        # ce n'est pas la peine de recréer un indexed_object, il suffit d'ajouter les annotations des symboles hypothese
        # aux annotation du symbole reference avec lequel ils sont associés
        (ref_stream, hyp_stream) = aligned_streams
        for i in range( 0, len( ref_stream )):
        ##        print("****************************")
        ##        print(ref_stream[ i ])
            aligned_symbol_tuple = ref_stream[ i ]
        ##        print( type( aligned_symbol_tuple ))
        ##        pprint( aligned_symbol_tuple )
        ##        print( len( aligned_symbol_tuple ) )
            (ref_isym, ref_atom_span_lst, hyp_isym_span_lst, hyp_atom_span_lst ) = aligned_symbol_tuple
            assert( type( ref_isym ) is indexed_object )
        ##        print( 'merging annotation of ref symbol {0} whose annotations are {1}'.format( ref_isym, ref_isym.annotations))
            merged_hyp_annotations = {}
            compteur = 0
            for hyp_isym_span in hyp_isym_span_lst:
                a_hyp_lst = []
                all_hyp_annotation_keys = []
                for hyp_isym in hyp_isym_span:
                    compteur += 1
##                    print( '\t\twith hyp symbol {0} annotations {1}'.format( hyp_isym, hyp_isym.annotations ))
                    assert( type( hyp_isym ) is indexed_object )
                    for k in hyp_isym.annotations.keys():
                        if k not in all_hyp_annotation_keys:
                                all_hyp_annotation_keys.append( k )
                        if k not in merged_hyp_annotations.keys():
                                merged_hyp_annotations[k] = []
##                        print( 'DEBUG>>>>merging annotation == {0}'.format( hyp_isym.annotations[ k ] ))
                # for all keys in the annotations symbol lst, merge all the annotations values associated to the key
                # if any, for each hyp symbol, and merge them (first stage, of annotation merging, from the hyp stream,
                # e.g. computing a softmax of BERT embedding for a sequence of hyp BERT tokens associated to a single ref symbol
                for k in all_hyp_annotation_keys:
                        hyp_annotation_value_lst = []
                        for hyp_isym_span in hyp_isym_span_lst:
                                for hyp_isym in hyp_isym_span:
                                        if k in hyp_isym.annotations.keys():
                                                hyp_annotation_value_lst.append( hyp_isym.annotations[ k ] )
                        # then apply the hyp_annotation_merger on the value list and transfer it to the ref stream
                        # associated to the unique reference symbol
                        merged_hyp_annotations[ k ] = hyp_lst_annotation_mixer( k, hyp_annotation_value_lst )
            # now transfer the hyp annotation merged into a single annotation per symbol for all the existing hyp annotation keys
            # onto the unique ref symbol, if the annotation does not exist on the reference side, the hypothesis
            # annotation is simply added, other if the key exists on both streams (i.e. reference and hypothesis)
            # the annotation_mixer() function is used to combine the reference and the hypotesis (possiblibly already merged
            # from the hypothesis side) into a single reference annotation for the given annotation key.
            for k in merged_hyp_annotations.keys():
                if k not in ref_isym.annotations.keys():
                    ref_isym.annotations[ k ] = merged_hyp_annotations[ k ]
                else:
                    ref_isym.annotations[ k ] = annotation_mixer( k, ref_isym.annotations[ k ], merged_hyp_annotations[ k ] )
        ##    print( '=====merged annotation of ref symbol {0} whose annotations are {1}'.format( ref_isym, ref_isym.annotations))
        return ref_stream # le flux d'alignement contient maintenant par effet de bord les annotations d'hypothèse qui ont été fusionnées avec les annotations de ref
    
#==========

def indexed_object_to_mwu( doc, realigned_streams, pointers_mwu, sentence_tokens_ids ):
    # WARNING doc is a construction and not a doc !
    sentence_tokens_ids[ doc.id ] = {}
    pointers_mwu[ doc.id ] = {}
    for i in range( len( realigned_streams ) ):
        tok = realigned_streams[ i ][ 0 ]
        tok_mwu = mwu( nm = 'T' + '_' + str( tok.annotations[ 'sent_id' ]) + '_' + str( tok.annotations[ 'id' ] ),
                        txtsps = [ ( tok.annotations[ 'start_char' ], tok.annotations[ 'end_char' ] ) ],
                        annotations = tok.annotations,
                        typ = "Token" )

        doc.add_mwu( tok_mwu )
    
        pointers_mwu[ doc.id ][ tok_mwu.name ] = tok_mwu
        sent_id = tok.annotations[ 'sent_id' ]
        if( sent_id not in sentence_tokens_ids[ doc.id ].keys() ):
            sentence_tokens_ids[ doc.id ][ sent_id ] = []
        sentence_tokens_ids[ doc.id ][ sent_id ].append( tok_mwu.name )

def get_relations( doc, pointers_mwu, sentence_tokens_ids ):
        if STANZA:
                for k in sentence_tokens_ids[doc.id].keys():
                        #        print("======================================================")
                        for mwu_pointer in sentence_tokens_ids[doc.id][k]:
                            w_heads = pointers_mwu[doc.id][mwu_pointer].annotations["w_head"]
                            for idx in range(len(w_heads)):
                                 if w_heads[idx] == 0 :
                                     rel = relation(nm = None,
                                                    src=pointers_mwu[doc.id][mwu_pointer], 
                                                    trg=pointers_mwu[doc.id][mwu_pointer],
                                                    annotations = {},
                                                    typ=pointers_mwu[doc.id][mwu_pointer].annotations["w_deprel"][idx] )
                        #                    print()
                        #                    print('SRC:{0} : {1} ({2}) ||  TRG:{3} : {4} ({5}) || type: {6}'.format(rel.src.name, 
                        #                         pointers_mwu[doc.id][rel.src.name].annotations['text'], pointers_mwu[doc.id][rel.src.name].annotations['w_upos'], 
                        #                         rel.trgt.name, pointers_mwu[doc.id][rel.trgt.name].annotations['text'], 
                        #                         pointers_mwu[doc.id][rel.trgt.name].annotations['w_upos'],rel.typ))
                                     doc.add_rel(rel)
                                 else:
                                    for mwuP in sentence_tokens_ids[doc.id][k]:
                                        if ( pointers_mwu[doc.id][mwuP].annotations["id"] == w_heads[idx] ):
                                            rel = relation(nm = None,
                                                           src=pointers_mwu[doc.id][mwu_pointer], 
                                                           trg=pointers_mwu[doc.id][mwuP],
                                                           annotations = {},
                                                           typ=pointers_mwu[doc.id][mwu_pointer].annotations["w_deprel"][idx])
                                            
                        #                            print()
                        #                            print('SRC:{0} : {1} ({2}) ||  TRG:{3} : {4} ({5}) || type: {6}'.format(rel.src.name, 
                        #                                pointers_mwu[doc.id][rel.src.name].annotations['text'], 
                        #                                pointers_mwu[doc.id][rel.src.name].annotations['w_upos'], rel.trgt.name, 
                        #                                pointers_mwu[doc.id][rel.trgt.name].annotations['text'], pointers_mwu[doc.id][rel.trgt.name].annotations['w_upos'],rel.typ))
                                            doc.add_rel(rel)
        elif SPACY:
                for k in sentence_tokens_ids[doc.id].keys():
                        #        print("======================================================")
                        for mwu_pointer in sentence_tokens_ids[doc.id][k]:
                                head = pointers_mwu[doc.id][mwu_pointer].annotations[ "head" ]
                                if head == 0 :
                                        rel = relation(nm = None,
                                                    src=pointers_mwu[doc.id][mwu_pointer], 
                                                    trg=pointers_mwu[doc.id][mwu_pointer],
                                                    annotations = {},
                                                    typ=pointers_mwu[doc.id][mwu_pointer].annotations["deprel"] )
                                        if DEBUG:
                                                print()
                                                print('SRC:{0} : {1} ({2}) ||  TRG:{3} : {4} ({5}) || type: {6}'.format(rel.src.name,
                                                                                                                        pointers_mwu[doc.id][rel.src.name].annotations['text'],
                                                                                                                        pointers_mwu[doc.id][rel.src.name].annotations['pos'],
                                                                                                                        rel.trgt.name, pointers_mwu[doc.id][rel.trgt.name].annotations['text'],
                                                                                                                        pointers_mwu[doc.id][rel.trgt.name].annotations['upos'],
                                                                                                                        rel.typ))
                                        doc.add_rel(rel)
                                else:
                                        for mwuP in sentence_tokens_ids[doc.id][k]:
                                                if ( pointers_mwu[doc.id][mwuP].annotations["id"] == head ):
                                                    rel = relation(nm = None,
                                                                   src=pointers_mwu[doc.id][mwu_pointer], 
                                                                   trg=pointers_mwu[doc.id][mwuP],
                                                                   annotations = {},
                                                                   typ=pointers_mwu[doc.id][mwu_pointer].annotations["deprel"])
                                                    if DEBUG:
                                                            print()
                                                            print('SRC:{0} : {1} ({2}) ||  TRG:{3} : {4} ({5}) || type: {6}'.format(rel.src.name,
                                                                                                                                   pointers_mwu[doc.id][rel.src.name].annotations['text'],
                                                                                                                                   pointers_mwu[doc.id][rel.src.name].annotations['pos'],
                                                                                                                                   rel.trgt.name,
                                                                                                                                   pointers_mwu[doc.id][rel.trgt.name].annotations['text'],
                                                                                                                                   pointers_mwu[doc.id][rel.trgt.name].annotations['pos'],
                                                                                                                                   rel.typ))
                                                    doc.add_rel( rel )
        else:
                pass
        

